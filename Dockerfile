from python
workdir /app
copy requirements.txt requirements.txt
run pip3 install -r requirements.txt
copy . .
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]